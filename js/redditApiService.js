/**
 * Created by Admin on 20.02.2017.
 */
/**
 * Laravel Reddit Api Service
 */
var RedditApiService = function () {

    this.api_endpoint = config.web_application_url + '/api/v1';

    this.reddit_url = 'https://www.reddit.com';

    this.checkApiUser = function (userData) {

        var deferred = jQuery.Deferred();

        var url = this.api_endpoint + '/check-user';

        jQuery.ajax({
            url:url,
            type: "GET",
            data: userData,
            success:function(response){
                deferred.resolve(response);
            },
            error: function(error){
                deferred.reject(error);
            }
        });
        return deferred.promise();
    };
    this.apiLogin = function(request){
        var deferred = jQuery.Deferred();

        var url = this.reddit_url + '/api/login';

        jQuery.ajax({
            url: url,
            type: "POST",
            data: request,
            success:function(response){
                deferred.resolve(response);
            },
            error: function(error){
                deferred.reject(error);
            }
        });
        return deferred.promise();
    };
    this.apiMeJson = function(headerParams){

        var deferred = jQuery.Deferred();

        var url = this.reddit_url + '/api/me.json';

        jQuery.ajax({
            url: url,
            type: "GET",
            headers: headerParams,
            success:function(response){
                deferred.resolve(response);
            },
            error: function(error){
                deferred.reject(error);
            }
        });
        return deferred.promise();
    };
    this.addPost = function(popstData){

        var deferred = jQuery.Deferred();

        var url = this.api_endpoint + '/add-post';

        jQuery.ajax({
            url: url,
            type: "POST",
            data: popstData,
            success:function(response){
                deferred.resolve(response);
            },
            error: function(error){
                deferred.reject(error);
            }
        });
        return deferred.promise();
    };
    this.addSubreddit = function(subredditData){

        var deferred = jQuery.Deferred();

        var url = this.api_endpoint + '/add-subreddit';

        jQuery.ajax({
            url: url,
            type: "POST",
            data: subredditData,
            success:function(response){
                deferred.resolve(response);
            },
            error: function(error){
                deferred.reject(error);
            }
        });
        return deferred.promise();
    };
    this.removePost = function(postId,userId, typeId, access_token){

        var deferred = jQuery.Deferred();

        var url = this.api_endpoint + '/remove-post';

        jQuery.ajax({
            url: url,
            type: "POST",
            data: {
                post_id:postId,
                user_id:userId,
                type_id: typeId,
                access_token:access_token
            },
            success:function(response){
                deferred.resolve(response);
            },
            error: function(error){
                deferred.reject(error);
            }
        });
        return deferred.promise();
    };
    this.removeSubreddit = function(subredditId,userId, access_token){

        var deferred = jQuery.Deferred();

        var url = this.api_endpoint + '/remove-subreddit';

        jQuery.ajax({
            url: url,
            type: "POST",
            data: {
                subredditId:subredditId,
                user_id:userId,
                access_token:access_token
            },
            success:function(response){
                deferred.resolve(response);
            },
            error: function(error){
                deferred.reject(error);
            }
        });
        return deferred.promise();
    }
};