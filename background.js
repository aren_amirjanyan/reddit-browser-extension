/**
 * Created by Admin on 17.02.2017.
 */

chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
    switch (message.type) {
        case 'checkCookies':
        {
            checkCookies(
                'reddit_session',
                function (cookie) {
                    if(cookie && cookie.value){
                        sendResponse({
                            data: cookie,
                            success: true
                        });
                    }else{
                        sendResponse({
                            data: [],
                            success: false
                        });
                    }
                },
                function () {
                    sendResponse({
                        data: [],
                        success: false
                    });
                }
            );
            break;
        }
        case 'checkCookieOfApp':
        {
            checkCookies(
                'reddit_user',
                function (cookie) {
                    if(cookie && cookie.value){
                        sendResponse({
                            data: cookie,
                            success: true
                        });
                    }else{
                        sendResponse({
                            data: [],
                            success: false
                        });
                    }
                },
                function () {
                    sendResponse({
                        data: [],
                        success: false22
                    });
                }
            );
            break;
        }
        case 'ApiLogin':{
            var request = message.request;
            var redditApiService = new RedditApiService();
            redditApiService.apiLogin(request).then(
                function(response){
                    if(response.json.errors.length == 0 && response.json.data.modhash)
                    {
                        var modhash = response.json.data.modhash;

                        var headerParams = {
                            'X-Modhash':modhash
                        };
                        redditApiService.apiMeJson(headerParams).then(
                            function(response){
                                sendResponse({
                                    data: response,
                                    success: true
                                });
                            },
                            function(error){
                                sendResponse({
                                    data: error,
                                    success: false
                                });
                            }
                        ).catch(function(error){
                                sendResponse({
                                    data: error,
                                    success: false
                                });
                        });
                    }

                },
                function(error){
                    sendResponse({
                        data: error,
                        success: false
                    });
                }
            ).catch(function(error){
                    sendResponse({
                        data: error,
                        success: false
                    });
            });
            break;
        }
        case 'checkUser':
        {
            var userBrowserData = message.request;
            var redditApiService = new RedditApiService();
            redditApiService.checkApiUser(userBrowserData).then(
                function (response) {
                    if (response && response.data && response.success) {
                        var userData = getUserData(response.data);
                        sendResponse({
                            data: userData,
                            success: true
                        });
                    } else {
                        sendResponse({
                            data: response,
                            success: false
                        });
                    }
                },
                function (error) {
                    sendResponse({
                        data: error,
                        success: false
                    });
                }
            ).catch(function (error) {
                    sendResponse({
                        data: error,
                        success: false
                    });
                });
            break;
        }
        case 'addPost':
        {
            checkCookies(
                'reddit_user',
                function(cookie){
                    var request = message.request;
                    var userData = $.parseJSON(cookie.value);
                    var userId = userData.user_id;
                    var access_token = userData.access_token;
                    var redditApiService = new RedditApiService();
                    request.user_id = userId;
                    request.access_token = access_token;
                    redditApiService.addPost(request).then(
                        function(response){
                            if(response && response.data && response.success){
                                var userData = getUserData(response.data);
                                userData.new_archive_posts_type = response.data.new_archive_posts_type;
                                sendResponse({
                                    data: userData,
                                    success: true
                                });
                            }else{
                                sendResponse({
                                    data: response,
                                    success: false
                                });
                            }
                        },
                        function(error){
                            sendResponse({
                                data: error,
                                success: false
                            });
                        }).catch(function(error){
                            sendResponse({
                                data: error,
                                success: false
                            });
                        });
                },
                function(){
                    sendResponse({
                        data: [],
                        success: false
                    });
                }
            );
            break;
        }
        case 'addSubreddit':
        {
            checkCookies(
                'reddit_user',
                function(cookie){
                    var request = message.request;
                    var userData = $.parseJSON(cookie.value);
                    var userId = userData.user_id;
                    var access_token = userData.access_token;
                    var redditApiService = new RedditApiService();
                    request.user_id = userId;
                    request.access_token = access_token;
                    redditApiService.addSubreddit(request).then(
                        function(response){
                            if(response && response.data && response.success){
                                var userData = getUserData(response.data);
                                sendResponse({
                                    data: userData,
                                    success: true
                                });
                            }else{
                                sendResponse({
                                    data: response,
                                    success: false
                                });
                            }
                        },
                        function(error){
                        sendResponse({
                            data: error,
                            success: false
                        });
                    }).catch(function(error){
                        sendResponse({
                            data: error,
                            success: false
                        });
                    });
                },
                function(){
                    sendResponse({
                        data: [],
                        success: false
                    });
                }
            );
            break;
        }
        case 'removePost':
        {
            checkCookies(
                'reddit_user',
                function(cookie){
                    var postId = message.request.request_id;
                    var typeId = message.request.type_id;
                    var userData = $.parseJSON(cookie.value);
                    var userId = userData.user_id;
                    var access_token = userData.access_token;
                    var redditApiService = new RedditApiService();
                    redditApiService.removePost(postId,userId,typeId,access_token).then(
                        function(response){
                            if(response && response.data && response.success){
                                var userData = getUserData(response.data);
                                sendResponse({
                                    data: userData,
                                    success: true
                                });
                            }else{
                                sendResponse({
                                    data: response,
                                    success: false
                                });
                            }
                        },
                        function(error){
                            sendResponse({
                                data: error,
                                success: false
                            });
                        }
                    ).catch(function(error){
                            sendResponse({
                                data: error,
                                success: false
                            });
                        });
                },
                function(){
                    sendResponse({
                        data: [],
                        success: false
                    });
                }
            );
            break;
        }
        case 'removeSubreddit':
        {
            checkCookies(
                'reddit_user',
                function(cookie){
                    var subredditId = message.request.request_id;
                    var userData = $.parseJSON(cookie.value);
                    var userId = userData.user_id;
                    var access_token = userData.access_token;
                    var redditApiService = new RedditApiService();
                    redditApiService.removeSubreddit(subredditId,userId, access_token).then(
                        function(response){
                            if(response && response.data && response.success){
                                var userData = getUserData(response.data);
                                sendResponse({
                                    data: userData,
                                    success: true
                                });
                            }else{
                                sendResponse({
                                    data: response,
                                    success: false
                                });
                            }
                        },
                        function(error){
                            sendResponse({
                                data: error,
                                success: false
                            });
                        }
                    ).catch(function(error){
                            sendResponse({
                                data: error,
                                success: false
                            });
                    });
                },
                function(){
                    sendResponse({
                        data: [],
                        success: false
                    });
                }
            );
            break;
        }
    }
    return true;
});

function checkCookies(cookieName, successCallback, errorCallback) {
    chrome.cookies.get({
        url: 'https://www.reddit.com/',
        name: cookieName
    }, function (cookie) {
        if (cookie) {
            successCallback(cookie);
        } else {
            errorCallback();
        }
    });
}
function getUserData(data){
    var userData = {
        reddit_id: data.reddit_id,
        name: data.name,
        reddit_username: data.reddit_username,
        email: data.email,
        access_token: data.access_token,
        refresh_token: data.refresh_token,
        token_type: data.token_type,
        subreddits:data.subreddits,
        posts:data.posts,
        archive_posts_types:data.archive_posts_types,
        posts_types:data.posts_types
    };
    return userData;
}

