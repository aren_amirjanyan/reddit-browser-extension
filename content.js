/**
 * Created by Admin on 17.02.2017.
 */
$(document).ready(function () {
    checkCookies();
    /**
     * functionality of login to apllication
     */
    $(document).on('click','#login-reddit',function(){
        var username = $('#username').val();
        var password = $('#password').val();
        var api_type = 'json';
        chrome.runtime.sendMessage({
            type: 'ApiLogin',
            request:{
                user:username,
                passwd:password,
                api_type:api_type
            }
        }, function (response) {
            var userData = {
                user_id:'',
                user_name:'',
                user_modhash:''
            };
            if(response && response.success && response.data){
                userData.user_id = response.data.data.id;
                userData.user_name = response.data.data.name;
                userData.user_modhash = response.data.data.modhash;
                checkUser(userData);
            }
        });
    });
    /**
     * functionality of adding the data to application of buttons
     */
    $(document).on('click','.add-to-application',function(){
        $(this).attr('disabled',true);
        var currentElement = $(this);
        var  data = {};
        var typeOfData = false;
        if(currentElement.attr('data-type')){
            if(currentElement.attr('data-type') == 'subreddit'){
                data = {
                    fullname : currentElement.attr('data-fullname'),
                    display_name:currentElement.attr('data-display-name')
                };
                typeOfData = 'addSubreddit';
            } else {
                data = {
                    fullname : currentElement.attr('data-fullname'),
                    archive_post_type:currentElement.attr('data-archive-posts-type')
                };
                typeOfData = 'addPost';
            }
        }
        addToApplication(typeOfData,data);
    });
    /**
     * functionality of remove data from application of buttons
     */
    $(document).on('click','.remove-from-application',function(){
        $(this).attr('disabled',true);
        var type = $(this).attr('data-type');
        var dataId = $(this).attr('data-fullname');
        var typeOfData = false;
        var dataTypeId = null;
        switch (type){
            case 'post':{
                typeOfData = 'removePost';
                dataTypeId = $(this).attr('data-archive-posts-type');
                break;
            }
            case 'subreddit':{
                typeOfData = 'removeSubreddit';
                break;
            }
        }
        removeFromApplication(typeOfData,dataId,dataTypeId);
    });
    $(document).on('change','.select-box-archive-post-type',function(){
        var data_post_type = $(this).val();
        var data_post_id = $(this).attr('data-id');
        $('#button_' + data_post_id).attr('data-archive-posts-type',data_post_type);
        if(data_post_type != ''){
            $('#button_' + data_post_id).attr('disabled',false);
        }else{
            $('#button_' + data_post_id).attr('disabled',true);
        }
    });
});
/**
 *
 * @param typeOfData
 * @param data
 */
function addToApplication(typeOfData,data){

    var buttonName = '';
    var classOfButton = '';
    var nameOfButton = '';
    var nameOfType = '';

    switch (typeOfData){
        case 'addSubreddit':{
            nameOfType = 'subreddit';
            buttonName = 'Subreddit';
            break;
        }
        case 'addPost':{
            nameOfType = 'post';
            buttonName = 'Post';
            break;
        }
    }
    classOfButton = 'btn btn-danger remove-from-application';
    nameOfButton = 'Remove ' + buttonName + ' from application';
    chrome.runtime.sendMessage({
        type: typeOfData,
        request: data
    }, function (response) {
        if(response && response.success){
            setUserDataToCookie(response,'reddit_user');
            $('#button_'+data.fullname).attr('disabled',false);
            $('#button_'+data.fullname).attr('class',classOfButton);
            $('#button_'+data.fullname).text(nameOfButton);
            $('#select-box-'+data.fullname).parent().hide();
            //new_archive_posts_type
            if(response.data.new_archive_posts_type){
                $('.select-box-archive-post-type')
                    .append($("<option></option>")
                        .attr("value",response.data.new_archive_posts_type.id)
                        .text(response.data.new_archive_posts_type.title));
            }
        }else{
            alert('You can not add the ' + nameOfType +' in the application');
        }
    });
}
/**
 *
 * @param typeOfData
 * @param dataId
 */
function removeFromApplication(typeOfData,dataId,dataTypeId){

    var buttonName = '';
    var classOfButton = '';
    var nameOfButton = '';
    var nameOfType = '';
    var request = {};

    switch (typeOfData){
        case 'removeSubreddit':{
            nameOfType = 'subreddit';
            buttonName = 'Subreddit';
            request = {
                request_id:dataId
            };
            break;
        }
        case 'removePost':{
            nameOfType = 'post';
            buttonName = 'Post';
            request = {
                request_id:dataId,
                type_id:dataTypeId
            };
            break;
        }
    }

    classOfButton = 'btn btn-primary add-to-application';
    nameOfButton = 'Add ' + buttonName + ' to application';

    chrome.runtime.sendMessage({
        type: typeOfData,
        request: request
    }, function (response) {
        if(response && response.success){
            setUserDataToCookie(response,'reddit_user');
            $('#button_'+dataId).attr('disabled',false);
            $('#button_'+dataId).attr('class',classOfButton);
            $('#button_'+dataId).text(nameOfButton);
            $('#select-box-'+dataId).parent().show();
        }else{
            alert('You can not remove the ' + nameOfType +' from application');
        }
    });
}
/**
 *
 * @param cname
 * @param cvalue
 * @param exdays
 */
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}
/**
 *
 * @param cname
 * @returns {*}
 */
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}
/**
 *
 * @param name
 */
function deleteCookie(name) {
    setCookie(name, "", {
        expires: -1
    })
}
function modalHTML(form) {
    var html = '<div class="modal fade" id="login-reddit-modal" role="dialog">' +
        '<div class="modal-dialog">' +
        '<!-- Modal content-->' +
        '<div class="modal-content">' +
        '<div class="modal-header" style="position:static">' +
        '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
        '<h4 class="modal-title">Reddit Extension</h4>' +
        '</div>' +
        '<div class="modal-body" style="padding:20px 60px 20px 60px;">' + form +
        '</div>' +
        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    return html;
}
function formHTML() {

    var html = '<h2 style="font-size: 30px;margin-top: 20px;margin-bottom: 30px;">Login Reddit Application</h2>' +
        '<form class="form-horizontal">' +
        '<div class="form-group">' +
        '<label class="control-label col-sm-2" style="font-size: 14px;" for="username">Username:</label>' +
        '<div class="col-sm-10">' +
        '<input type="text" class="form-control" id="username" placeholder="Enter Reddit username">' +
        '</div>' +
        '</div>' +
        '<div class="form-group">' +
        '<label class="control-label col-sm-2" style="font-size: 14px;" for="password">Password:</label>' +
        '<div class="col-sm-10">' +
        '<input type="password" class="form-control" id="password" placeholder="Enter Reddit password">' +
        '</div>' +
        '</div>' +
        '<div class="form-group">' +
        '<div class="col-sm-offset-2 col-sm-10">' +
        '<button type="button" id="login-reddit" class="btn btn-primary">Login</button>' +
        '</div>' +
        '</div>' +
        '</form>';
    return html;

}
function alertLoginRedditHtml() {
    var html = '<div class="modal fade" id="alert-login-reddit" role="dialog">' +
        '<div class="modal-dialog">' +
        '<div class="modal-content">' +
        '<div class="modal-header" style="position:static">' +
        '<button type="button" class="close" data-dismiss="modal">&times;</button>' +
        '<h4 class="modal-title">Login to Reddit</h4>' +
        '</div>' +
        '<div class="modal-body" style="padding:20px 60px 20px 60px;">'+
        '<p style="font-size: 14px">Please login in Reddit for adding the posts and subreddits to application</p>'+
        '<a style="font-size: 14px" href="https://www.reddit.com/login">Reddit Login</a>'+
        '</div>' +
        '<div class="modal-footer">' +
        '<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
    return html;
}
function checkCookies() {
    chrome.runtime.sendMessage({
        type: 'checkCookies',
        request: {}
    }, function (data) {
        if (data.success) {
            checkCookieOfApp();
        }else{
            deleteCookie('reddit_user');
            if(window.location.href != 'https://www.reddit.com/login'){
                var alert_login_reddit = alertLoginRedditHtml();
                $('body').append(alert_login_reddit);
                $('#alert-login-reddit').modal();
            }
        }
    });
}
function checkCookieOfApp(){
    chrome.runtime.sendMessage({
        type: 'checkCookieOfApp',
        request: {}
    }, function (data) {
            if(!data.success){
                if($('.add-to-application')){
                    $('.add-to-application').remove();
                }
                if(window.location.origin + window.location.pathname != 'https://www.reddit.com/api/v1/authorize'){
                    var form = formHTML();
                    var login_modal_html = modalHTML(form);
                    $('body').append(login_modal_html);
                    $('#login-reddit-modal').modal();
                }
            } else {
                var reddit_user = $.parseJSON(data.data.value);
                checkUser(reddit_user);
            }
    });
}
function checkUser(reddit_user){
    var userData = {
        user_id:'',
        name:'',
        user_name:'',
        email:'',
        access_token:'',
        refresh_token:'',
        token_type:''
    };
    chrome.runtime.sendMessage({
        type: 'checkUser',
        request:reddit_user
    }, function (response) {
        if(response && response.success){
            setUserDataToCookie(response,'reddit_user');
            $('#login-reddit-modal').modal('hide');
            var postOrSubredditParentelement = $('.thing');
            var searchResultsPostOrSubredditParentelement = $('.search-result');
            var subreddits = response.data.subreddits;
            var posts = response.data.posts;
            var archivePostsTypes = response.data.archive_posts_types;
            var postsTypes = response.data.posts_types;
            var archivePostsTypesSelectBoxOptions = '';
            if(archivePostsTypes.length > 0){
                $.each(archivePostsTypes,function(keyItem,item){
                    archivePostsTypesSelectBoxOptions += '<option value="'+item.id+'">'+item.title+'</option>';
                });
            }
            $.each(postOrSubredditParentelement,function(key,value){
                if($(value).attr('data-type') && $(value).attr('data-type') != 'comment' && $(value).attr('data-type') != 'morechildren' && $(value).attr('data-type') != 'morerecursion'){
                    var fullname = $(value).attr('data-fullname');
                    var dataDisplayName = '';
                    var elementId = 'button_' + $(value).attr('data-fullname');
                    var buttonName = '';
                    var select_box_for_posts_types = '';
                    var dataArchivePostType = '';
                    var disabled = '';
                    if($(value).attr('data-type') == 'subreddit'){
                        var displayName = $(value).children('div.midcol').children('span.fancy-toggle-button').attr('data-sr_name');
                        dataDisplayName = 'data-display-name="'+displayName+'"';
                        var buttonName = 'Subreddit';
                        var type = 'subreddit';
                        var classOfButton = '';
                        var nameOfButton = '';
                        if(subreddits.indexOf(fullname) != -1){
                            classOfButton = 'btn btn-danger remove-from-application';
                            nameOfButton = 'Remove ' + buttonName + ' from application';

                        }else{
                            classOfButton = 'btn btn-primary add-to-application';
                            nameOfButton = 'Add ' + buttonName + ' to application';
                        }
                    }else{
                        console.log($(value).attr('data-type'));
                        var buttonName = 'Post';
                        var type = 'post';
                        var classOfButton = '';
                        var nameOfButton = '';
                        dataArchivePostType = 'data-archive-posts-type="default"';
                        var displayOfSelectBox = 'display: block';
                        if(posts.indexOf(fullname) != -1){
                            dataArchivePostType = 'data-archive-posts-type="'+postsTypes[$(value).attr('data-fullname')]+'"';
                            classOfButton = 'btn btn-danger remove-from-application';
                            nameOfButton = 'Remove ' + buttonName + ' from application';
                            displayOfSelectBox = 'display: none';
                        }else{
                            classOfButton = 'btn btn-primary add-to-application';
                            nameOfButton = 'Add ' + buttonName + ' to application';
                            disabled = 'disabled="disabled"';
                            displayOfSelectBox = 'display: block';
                        }
                        var selectBoxId = 'select-box-'+$(value).attr('data-fullname');
                        select_box_for_posts_types = '<div class="col-md-2" style="'+displayOfSelectBox+'"><select id="'+selectBoxId+'" data-id="'+$(value).attr('data-fullname')+'" class="form-control select-box-archive-post-type" multiple>'+archivePostsTypesSelectBoxOptions+'</select></div>';
                    }
                    $(value).append('<form class="form-inline">'+select_box_for_posts_types+'<div class="col-md-2"><button type="button" id="'+elementId+'" data-fullname="'+fullname+'" '+dataArchivePostType+' data-type="'+type+'" '+dataDisplayName+' class="'+classOfButton+'" style="position: relative;" '+disabled+'>'+nameOfButton+'</button></div></form>');
                }
            });
            $.each(searchResultsPostOrSubredditParentelement,function(key,value){
                if($(value).attr('data-fullname')){
                    var fullname = $(value).attr('data-fullname');
                    var dataDisplayName = '';
                    var elementId = 'button_' + $(value).attr('data-fullname');
                    var buttonName = '';
                    var select_box_for_posts_types = '';
                    var dataArchivePostType = '';
                    var disabled = '';
                    if($(value).hasClass("search-result-subreddit")){
                        var displayName = $(value).children('div.search-result-meta').children('span.fancy-toggle-button').attr('data-sr_name');
                        dataDisplayName = 'data-display-name="'+displayName+'"';
                        var buttonName = 'Subreddit';
                        var type = 'subreddit';
                        var classOfButton = '';
                        var nameOfButton = '';
                        if(subreddits.indexOf(fullname) != -1){
                            classOfButton = 'btn btn-danger remove-from-application';
                            nameOfButton = 'Remove ' + buttonName + ' from application';
                        }else{
                            classOfButton = 'btn btn-primary add-to-application';
                            nameOfButton = 'Add ' + buttonName + ' to application';
                        }
                        $(value).append('<button type="button" id="'+elementId+'" data-fullname="'+fullname+'" data-type="'+type+'" '+dataDisplayName+' class="'+classOfButton+'" style="position: relative;">'+nameOfButton+'</button>');
                    }else{
                        var buttonName = 'Post';
                        var type = 'post';
                        var classOfButton = '';
                        var nameOfButton = '';
                        dataArchivePostType = 'data-archive-posts-type="default"';
                        var displayOfSelectBox = 'display: block';

                        if(posts.indexOf(fullname) != -1){
                            dataArchivePostType = 'data-archive-posts-type="'+postsTypes[$(value).attr('data-fullname')]+'"';
                            classOfButton = 'btn btn-danger remove-from-application';
                            nameOfButton = 'Remove ' + buttonName + ' from application';
                            displayOfSelectBox = 'display: none';
                        }else{
                            classOfButton = 'btn btn-primary add-to-application';
                            nameOfButton = 'Add ' + buttonName + ' to application';
                            disabled = 'disabled="disabled"';
                            displayOfSelectBox = 'display: block';
                        }
                        var selectBoxId = 'select-box-'+$(value).attr('data-fullname');
                        select_box_for_posts_types = '<div class="col-md-2" style="'+displayOfSelectBox+'"><select id="'+selectBoxId+'" data-id="'+$(value).attr('data-fullname')+'" class="form-control select-box-archive-post-type" multiple>'+archivePostsTypesSelectBoxOptions+'</select></div>';

                        $(value).append('<form class="form-inline">'+select_box_for_posts_types+'<div class="col-md-2" style="margin-left: 120px;"><button type="button" id="'+elementId+'" data-fullname="'+fullname+'" '+dataArchivePostType+' data-type="'+type+'" '+dataDisplayName+' class="'+classOfButton+'" style="position: relative;" '+disabled+'>'+nameOfButton+'</button></div></form>');
                    }
                }
            });
            $('.select-box-archive-post-type').select2({
                placeholder: "Choose Archive Post Type",
                maximumSelectionLength: 1,
                tags: true
            });
        } else {
            deleteCookie('reddit_user');
            if(window.location.origin + window.location.pathname != 'https://www.reddit.com/api/v1/authorize'){
                registerRedditInApplication(true);
            }
        }
    });
}
function registerRedditInApplication(show){
    if(show){
        var parameters = "location=yes,top=100,left=600,width=600,height=700,resizable=yes,menubar=yes,toolbar=yes,sscrollbars=yes";

        var url = config.web_application_url + "/auth/register?referrer=extension";

        var newWindowName = 'Access to Reddit App';

        window.open(url, newWindowName, parameters);
    }else{
        alert('close');
    }
}
function registerInReddit(){

    var parameters = "location=yes,top=100,left=600,width=600,height=700,resizable=yes,menubar=yes,toolbar=yes,sscrollbars=yes";

    var url = "https://www.reddit.com/login";

    var newWindowName = 'Login To Reddit';

    window.open(url, newWindowName, parameters);
}
function setUserDataToCookie(response,cookie_name){
    var userData = {
    user_id         : response.data.reddit_id,
    name            : response.data.name,
    user_name       : response.data.reddit_username,
    email           : response.data.email,
    access_token    : response.data.access_token,
    refresh_token   : response.data.refresh_token,
    token_type      : response.data.token_type
    };
    setCookie(cookie_name, JSON.stringify(userData),1);
}
